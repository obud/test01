#/bin/sh

# Install packages
opkg update
opkg install wireguard wireguard-tools
 
# Configuration parameters
WG_IF="wgvpn"
WG_SERV="dev.amcus.cz"
WG_PORT="51901"
WG_PUB_SERVER="kCe/f8tH+ZL6hpa8j6LufMGKDHvEmuV49VaDsdCJVE4="


# Serial number
SN=$(printf "%012d" $(mnf_info -s))
WG_PREFIX6="fd00:${SN:0:4}:${SN:4:4}:${SN:8:4}"
WG_ADDR6="${WG_PREFIX6}::2/64"

# Generate keys
umask go=
wg genkey | tee wgclient.key | wg pubkey > wgclient.pub
 
# Client private key
WG_KEY_CLIENT="$(cat wgclient.key)"
WG_PUB_CLIENT="$(cat wgclient.pub)"


# Configure firewall
uci rename firewall.@zone[0]="lan"
uci rename firewall.@zone[1]="wan"
uci del_list firewall.wan.network="${WG_IF}"
uci add_list firewall.wan.network="${WG_IF}"
uci commit firewall
/etc/init.d/firewall restart


# Configure network
uci -q delete network.${WG_IF}
uci set network.${WG_IF}="interface"
uci set network.${WG_IF}.proto="wireguard"
uci set network.${WG_IF}.private_key="${WG_KEY_CLIENT}"
uci add_list network.${WG_IF}.addresses="${WG_ADDR6}"
 
# Add VPN peers
uci -q delete network.wgserver
uci set network.wgserver="wireguard_${WG_IF}"
uci set network.wgserver.public_key="${WG_PUB_SERVER}"
uci set network.wgserver.endpoint_host="${WG_SERV}"
uci set network.wgserver.endpoint_port="${WG_PORT}"
uci set network.wgserver.route_allowed_ips="1"
uci set network.wgserver.persistent_keepalive="25"
uci add_list network.wgserver.allowed_ips="${WG_PREFIX6}::/64"
uci commit network
/etc/init.d/network restart

echo ""
echo "[Peer]"
echo "PublicKey = ${WG_PUB_CLIENT}"
echo "AllowedIPs = ${WG_PREFIX6}::/64"
echo ""
